#!python

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt




nPoint = 101

sArray = range(0,nPoint)
pArray = [0]*nPoint
aArray = [1]*nPoint
aArrayNew1 = [1]*nPoint
aArrayNew2 = [1]*nPoint
p0 = 1.
u0 = 1.
rou0 = 1.
E = 1000
h0 = 0.001
pTotal = p0+ 0.5*rou0*u0*u0
a0 = 1.

def _main():
    _init()
    _calculateP()
    _calculateA()

    plt.show()
    print("hello blood")

def _init():

    for i in range(0,nPoint):
        aArray[i] = a0+0.1 * a0 * np.sin(i/25.*np.pi)
    plt.ylim(0,3)
    plt.plot(aArray)
    plt.plot(sArray)

def _calculateP():
    for i in range(0,nPoint):
        u = u0*a0 / aArray[i]
        pArray[i] = pTotal - 0.5*rou0*u*u

def _calculateA():
    for i in range(0,nPoint):
        t1 = (pArray[i] - p0) * np.sqrt(aArray[i]/np.pi) / E / h0 +1
        print(t1)
        aArrayNew1[i] = aArray[i] * t1 * t1
    plt.plot(aArrayNew1)



if __name__ == "__main__" :
    _main()







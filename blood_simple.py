import numpy as np

class vessel:
    def __init__(self):
        self.nPoint = 101
        self.sArray = range(0, self.nPoint)
        self.pArray = [1.] * self.nPoint
        self.p1Array = [1.] * self.nPoint
        self.aArrayOri = [1.] * self.nPoint
        self.aArray = [1.] * self.nPoint
        self.uArray = [1.] * self.nPoint
        self.fArray = [1.] * self.nPoint
        self.coszArray = [1.] * self.nPoint
        self.P0 = 100000. # out environment pressure of vessel
        self.Pout = self.P0
        self.Pin = self.P0
        self.ROU = 1050.
        self.E = 600000.
        self.NIU = 0.495
        self.h0 = 0.01
        self.a0 = 1.
        self.Q = 1
        self.G = 9.8
        self.fVisc = 0.02
        for i in range(0,self.nPoint):
            self.aArray[i] = self.a0+0.1 * self.a0 * np.sin(i/25.*np.pi)
            self.aArrayOri[i] = self.a0+0.1 * self.a0 * np.sin(i/25.*np.pi)

    def setSArray(self, vSArray):
        self.nPoint =  len(vSArray)
        #self.sArray.resize(self.nPoint)
        self.sArray = [0] * self.nPoint
        self.pArray = [1] * self.nPoint
        self.p1Array = [1] * self.nPoint
        self.uArray = [1] * self.nPoint
        for indx in range(self.nPoint):
            self.sArray[indx] = vSArray[indx]

    def getSArray(self):
        return tuple(self.sArray)

    def setAArray(self, vAArray):
        self.aArray = [1] * self.nPoint
        self.aArrayOri = [1] * self.nPoint
        for indx in range(self.nPoint):
            self.aArray[indx] = vAArray[indx]
            self.aArrayOri[indx] = vAArray[indx]

    def getAArray(self):
        return tuple(self.aArray)

    def setFArray(self, vFArray):
        self.fArray = [0] * self.nPoint
        for indx in range(self.nPoint):
            self.fArray[indx] = vFArray[indx]
            
    def setCoszArray(self, vcoszArray):
        self.coszArray = [0] * self.nPoint
        for indx in range(self.nPoint):
            self.coszArray[indx] = vcoszArray[indx]

    def _calculateVelocity(self):
        for ipoint in range(0, self.nPoint):
            self.uArray[ipoint] = self.Q / self.aArray[ipoint]
    def setE(self, e):
        self.E = e

    def setG(self, g):
        self.G = g

    def setQ(self, q):
        self.Q = q

    def getQ(self):
        return self.Q

    def setP0(self, val_p0):
        self.P0 = val_p0

    def setPout(self, p):
        self.Pout = p

    def getPout(self):
        return self.Pout

    def getPin(self):
        return self.Pin

    def setPin(self, p):
        self.Pin = p

    def _calculatePressure(self):
        uend = self.Q / self.aArray[-1]
        pTotal = self.Pout + 0.5 * self.ROU * uend**2
        for ipoint in range(0, self.nPoint):
            self.p1Array[ipoint] = pTotal - 0.5 * self.ROU * self.uArray[ipoint] **2
        del uend

        pDeltaArray = [1] * self.nPoint
        for ipoint in range(0, self.nPoint - 1):
            area = 0.5 * (self.aArray[ipoint] + self.aArray[ipoint+1])
            radius = np.sqrt(area / np.pi)
            uAverage = 0.5 * (self.uArray[ipoint] + self.uArray[ipoint+1])
            pDeltaArray[ipoint] = self.fVisc * uAverage ** 2 / radius /4.
        del area
        del radius
        del uAverage

    def _calculatePressure2(self):
        uin = self.Q / self.ROU / self.aArray[0]
        pTotal = self.Pin + 0.5 * self.ROU * uin**2
        for ipoint in range(0, self.nPoint):
            self.p1Array[ipoint] = pTotal - 0.5 * self.ROU * self.uArray[ipoint] **2
        del uin

        pDeltaArray = [0] * self.nPoint
        for ipoint in range(0, self.nPoint - 1):
            area = 0.5 * (self.aArray[ipoint] + self.aArray[ipoint+1])
            radius = np.sqrt(area / np.pi)
            uAverage = 0.5 * (self.uArray[ipoint] + self.uArray[ipoint+1])
            pDeltaArray[ipoint+1] = (self.fArray[ipoint]+self.fArray[ipoint+1])/2 * uAverage ** 2 / radius / 4. \
                                    + (self.sArray[ipoint+1] - self.sArray[ipoint]) * self.G *self.ROU
        del area
        del radius
        del uAverage

        self.pArray[0] = self.Pin
        sum = 0
        for ipoint in range(0, self.nPoint ):
            sum = sum - pDeltaArray[ipoint]
            self.pArray[ipoint] = self.p1Array[ipoint] + sum


    def _calculateArea(self):
        radius =1
        for ipoint in range(0, self.nPoint):
            radius = np.sqrt(self.aArrayOri[ipoint] / np.pi)
            #self.aArray[ipoint] = ((self.pArray[ipoint] - self.P0) * (1-self.NIU**2) * radius / self.E / self.h0 + 1) ** 2
            # 简化血管壁厚度为血管0.1
            self.aArray[ipoint] = ((self.pArray[ipoint] - self.P0) * (1-self.NIU**2) / self.E / 0.1 + 1) ** 2
            self.aArray[ipoint] = self.aArray[ipoint] * self.aArrayOri[ipoint]
        del radius


    def calculate(self):
        for i in range(100):
            self._calculateVelocity()
            self._calculatePressure()
            self._calculateArea()
        self.Pin = self.pArray[0]


    def calculate2(self):
        for i in range(100):
            self._calculateVelocity()
            self._calculatePressure2()
            self._calculateArea()
        self.Pout = self.pArray[-1]
        #print(self.sArray)


    def show(self):
#        plt.ylim(0, 2)
#        plt.plot(self.uArray)
#        plt.plot(self.pArray)
        pass

    def writeDAT(self, fileName):
        file = open(fileName, 'w')
        file.write('VARIABLES = "S"\n')
        file.write('"AREA"\n')
        file.write('"PRESSURE"\n')
        file.write('"VELOCITY"\n')
        file.write('ZONE T="vessel"')
        file.write(" I="+str(self.nPoint)+", J=1, K=1, ZONETYPE=Ordered\n")
        file.write(' DATAPACKING=POINT\n')
        file.write(' DT=(SINGLE SINGLE SINGLE SINGLE )\n')
        #file.write(' DT=(DOUBLE DOUBLE DOUBLE DOUBLE )\n')
        for i in range(self.nPoint):
            #file.write(" "+str(self.sArray[i]) +" "+ str(self.aArray[i])+" "+str(self.pArray[i]) + " " + str(self.uArray[i]) + "\n")
            file.write(" "+format(self.sArray[i], ".3e") \
                       +" "+format(self.aArray[i], ".3e") \
                       +" "+format(self.pArray[i], ".3e") \
                       + " " + format(self.uArray[i], ".3e") + "\n")
        file.close()

def main():
    mVessel = vessel()
    mVessel.setQ(1.2)
    mVessel.calculate()
    #mVessel.show()
    mVessel.writeDAT('/Users/mx/Desktop/1.dat')

if __name__ == '__main__':
    main()

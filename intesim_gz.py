import vtk
from PyQt5 import QtCore, QtGui, Qt, uic
from PyQt5.QtWidgets import QFileDialog
import sys
import numpy as np
import blood_simple as blood

# import mainwindow

from vtk.qt.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor

qtCreatorFile = "windowDesign/mainwindow.ui"  # Enter file here.

Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)

# class MainWindow(mainwindow.Ui_MainWindow):

class MainWindow(Qt.QMainWindow, Ui_MainWindow):
    def __init__(self, parent=None):
        Qt.QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)

        # mui = Ui_MainWindow()
        self.setupUi(self)


        #print(vtk.vtkVersion().GetVTKVersion())
        self.textBrowser.append("INTESIM 1.0 ")

        self.frame = Qt.QFrame()
        self.vl = Qt.QVBoxLayout()
        self.vtkWidget = QVTKRenderWindowInteractor(self.frame)
        self.vl.addWidget(self.vtkWidget)

        self.ren = vtk.vtkRenderer()
        self.ren.GradientBackgroundOn()
        self.ren.SetBackground(0.8, 0.8, 0.9)
        self.ren.SetBackground2(0.5, 0.4, 0.9)
        self.ren.SetBackground(1, 1, 1)
        self.ren.SetBackground2(1, 1, 1)
        self.vtkWidget.GetRenderWindow().AddRenderer(self.ren)
        self.iren = self.vtkWidget.GetRenderWindow().GetInteractor()
        self.camera = self.ren.GetActiveCamera()
        #        self._initWidget()
        self.interactorStyle = vtk.vtkInteractorStyleSwitch()
        self.interactorStyle.SetCurrentStyleToTrackballCamera()
        self.iren.SetInteractorStyle(self.interactorStyle)

        self.NPPC = 41
        self.NC = 31
        self.multiper = 1

        self.treeTab = []
        self.cirTab = []
        self.blds = []

        self.ren.GetActiveCamera().SetParallelProjection(1)

        self.frame.setLayout(self.vl)
        self.setCentralWidget(self.frame)

        self.show()
        self.iren.Initialize()

        self.actionView_X.triggered.connect(self.viewXP)
        self.actionView_Y.triggered.connect(self.viewYP)
        self.actionView_Z.triggered.connect(self.viewZP)
        self.actionView_XM.triggered.connect(self.viewXM)
        self.actionView_YM.triggered.connect(self.viewYM)
        self.actionView_ZM.triggered.connect(self.viewZM)
        self.actionReset_Camera.triggered.connect(self.resetCamera)
        self.actionLoadMesh.triggered.connect(self.loadMesh)
        self.actionCompute.triggered.connect(self.compute)
        self.actionExport_Result.triggered.connect(self.exportResult)

        self._initWidget()
        self.viewYP()

    def addHeartBox(self):
        self.boxSource = vtk.vtkCubeSource()
        self.boxSource.SetCenter(0, 0, 0)
        self.boxSource.SetXLength(80)
        self.boxSource.SetYLength(80)
        self.boxSource.SetZLength(100)
        self.boxSource.Update()

        self.boxMapper = vtk.vtkPolyDataMapper()
        self.boxMapper.SetInputConnection(self.boxSource.GetOutputPort())
        self.boxMapper.Update()

        self.boxActor = vtk.vtkActor()
        self.boxActor.SetMapper(self.boxMapper)
        self.boxActor.GetProperty().SetColor(0.5, 0, 0)

        self.ren.AddActor(self.boxActor)

    def _initWidget(self):
        self.axesActor = vtk.vtkAxesActor()
        self.axes = vtk.vtkOrientationMarkerWidget()
        self.axes.SetOrientationMarker(self.axesActor)
        self.axes.SetInteractor(self.iren)
        self.axes.EnabledOn()
        self.axes.InteractiveOn()

    def addVessel(self):
        mPoints3 = vtk.vtkPoints()
        for iC in range(self.NC):
            mCir = vtk.vtkRegularPolygonSource()
            mCir.SetRadius(0.2)
            mCir.GeneratePolygonOff()
            mCir.SetNumberOfSides(self.NPPC)
            x = iC * 0.2
            y = np.sin(x)
            mTan = np.cos(x)

            mCir.SetNormal(1, 0, 0.0)
            mCir.SetCenter(0, 0, 0.0)
            mCir.Update()

            mTransformFilter = vtk.vtkTransformFilter()
            mTransform = vtk.vtkTransform()
            mTransform.Translate(x, y, 0)
            mTransform.RotateZ(np.arctan(mTan) / np.pi * 180.)
            mTransformFilter.SetTransform(mTransform)
            mTransformFilter.SetInputConnection(mCir.GetOutputPort())
            mTransformFilter.Update()
            mPoints3.InsertPoints(mPoints3.GetNumberOfPoints(), mCir.GetOutput().GetPoints().GetNumberOfPoints(), 0,
                                  mTransformFilter.GetOutput().GetPoints())

        mUnstructedGrid3 = vtk.vtkUnstructuredGrid()
        mUnstructedGrid3.SetPoints(mPoints3)
        print()
        for iC in range(self.NC - 1):
            for iP in range(self.NPPC):
                id0 = self.NPPC * iC + iP
                id1 = (self.NPPC * iC + iP + 1) % self.NPPC + id0 // self.NPPC * self.NPPC
                id3 = self.NPPC * iC + self.NPPC + iP
                id2 = (self.NPPC * iC + self.NPPC + iP + 1) % self.NPPC + id3 // self.NPPC * self.NPPC
                print(id0),
                print(id1),
                print(id2),
                print(id3)
                print()
                mUnstructedGrid3.InsertNextCell(vtk.VTK_QUAD, 4, (int(id0), int(id1), int(id2), int(id3)))

        # Create a mapper
        mMapper = vtk.vtkDataSetMapper()
        mMapper.SetInputData(mUnstructedGrid3)

        # Create an actor
        mActor = vtk.vtkActor()
        mActor.SetMapper(mMapper)
        mActor.GetProperty().SetColor(1, 0, 0)

        self.ren.AddActor(mActor)

    def addArrow(self):
        self.arrowSource = vtk.vtkArrowSource()
        # self.arrowSource.SetShaftRadius(0.5)
        # self.arrowSource.SetShaftResolution(31)
        # self.arrowSource.SetTipLength(10)
        # self.arrowSource.SetTipRadius(1)

        self.arrowSource.Update()

        self.arrowMapper = vtk.vtkPolyDataMapper()
        self.arrowMapper.SetInputConnection(self.arrowSource.GetOutputPort())
        self.arrowMapper.Update()

        self.arrowActor = vtk.vtkActor()
        self.arrowActor.SetMapper(self.arrowMapper)

        self.ren.AddActor(self.arrowActor)
        self.vtkWidget.update()

    def addStartBall(self):
        self.sphereSource = vtk.vtkSphereSource()
        self.sphereSource.SetCenter(0, 0, 0)
        self.sphereSource.SetRadius(0.3)
        self.sphereSource.SetPhiResolution(11)
        self.sphereSource.SetThetaResolution(11)
        self.sphereSource.Update()

        mMapper = vtk.vtkPolyDataMapper()
        mMapper.SetInputConnection(self.sphereSource.GetOutputPort())
        mMapper.Update()

        self.mStartSphereActor = vtk.vtkActor()
        self.mStartSphereActor.SetMapper(mMapper)
        self.mStartSphereActor.GetProperty().SetColor(0, 1, 0)

        self.ren.AddActor(self.mStartSphereActor)

    ###########################################################33

    def readFile(self, valFile):
        inpFile = open(valFile, 'r')
        [self.nVessel, self.nAdjoint] = [int(x) for x in inpFile.readline().split()]
        self.textBrowser.append("Number of vessel : " + str(self.nVessel))
        self.textBrowser.append("Number of adjoint : " + str(self.nAdjoint))

        for iP in range(self.nAdjoint):
            line = inpFile.readline().split()
            items = []
            items.append(int(line[0]))
            items.append(int(line[1]))
            items.append(int(line[2]))
            items.append(float(line[3]))
            items.append(float(line[4]))
            self.treeTab.append(items)
            del items

        for iV in range(self.nVessel):
            self.cirTab.append(0)

        for iV in range(self.nVessel):
            (index, nCir) = [int(x) for x in inpFile.readline().split()]
            cirs = []
            for iCir in range(nCir):
                cir = [float(x) for x in inpFile.readline().split()]
                cir[0] = cir[0] / 1000.
                cir[1] = cir[1] / 1000.
                cir[2] = cir[2] / 1000.
                cir[3] = np.sqrt(cir[3] / np.pi) / 1000.
                cirs.append(cir)
                del cir
            self.cirTab[index] = cirs
            del cirs

    def loadMesh(self):
        mypath = QFileDialog.getOpenFileName(
            self,
            "Open a mesh"
        )
        if mypath == ("", ""):
            return
        else:
            print(mypath)
            self.clear()
            self.readFile(mypath[0])
            self.creatVesselFromFile()
        pass

    def clear(self):
        self.treeTab = []
        self.cirTab = []
        self.nAdjoint = 0
        self.nVessel = 0
        actors = self.ren.GetActors()
        tempActor = actors.GetLastActor()
        while tempActor != None:
            self.ren.RemoveActor(tempActor)
            tempActor = actors.GetLastActor()
        self.vtkWidget.update()
        
    def exportResult(self):
        saveFileName = QFileDialog.getSaveFileName(self,"保存文件")
        if saveFileName == ("", "") or saveFileName == None:
            return
        else:
            expFile = open(saveFileName[0], 'w')
            expFile.write(self.textBrowser.toPlainText())
            expFile.close()
        pass

    ###########################################################33

    def norm3p(self, id, p0, p1, p2):
        x = [0, 0, 0]
        y = [0, 0, 0]
        z = [0, 0, 0]
        [x[0], y[0], z[0]] = p0[0:3]
        [x[1], y[1], z[1]] = p1[0:3]
        [x[2], y[2], z[2]] = p2[0:3]
        xyz = [x, y, z]

        norm = [0, 0, 0]
        for iDim in range(3):
            a = xyz[iDim][2] - xyz[iDim][1] * 2 + xyz[iDim][0]
            a = a / 2.
            b = xyz[iDim][1] - xyz[iDim][0] - a
            norm[iDim] = 2 * a * id + b

        lenth = norm[0] ** 2 + norm[1] ** 2 + norm[2] ** 2
        lenth = np.sqrt(lenth)

        norm = tuple([xx / lenth for xx in norm])
        return norm

    ########################################################

    def viewXP(self):
        self.camera.SetFocalPoint(0, 0, 0)
        self.camera.SetPosition((-1, 0, 0))
        self.camera.SetViewUp(0, 1, 0)
        self.ren.ResetCamera()
        self.vtkWidget.update()

    def viewYP(self):
        self.camera.SetFocalPoint(0, 0, 0)
        self.camera.SetPosition((0, -1, 0))
        self.camera.SetViewUp(0, 0, 1)
        self.ren.ResetCamera()
        self.vtkWidget.update()

    def viewZP(self):
        self.camera.SetFocalPoint(0, 0, 0)
        self.camera.SetPosition((0, 0, -1))
        self.camera.SetViewUp(0, 1, 0)
        self.ren.ResetCamera()
        self.vtkWidget.update()

    def viewXM(self):
        self.camera.SetFocalPoint(0, 0, 0)
        self.camera.SetPosition((1, 0, 0))
        self.camera.SetViewUp(0, 1, 0)
        self.ren.ResetCamera()
        self.vtkWidget.update()

    def viewYM(self):
        self.camera.SetFocalPoint(0, 0, 0)
        self.camera.SetPosition((0, 1, 0))
        self.camera.SetViewUp(0, 0, 1)
        self.ren.ResetCamera()
        self.vtkWidget.update()

    def viewZM(self):
        self.camera.SetFocalPoint(0, 0, 0)
        self.camera.SetPosition((0, 0, 1))
        self.camera.SetViewUp(0, 1, 0)
        self.ren.ResetCamera()
        self.vtkWidget.update()

    def resetCamera(self):
        self.ren.ResetCamera()
        self.vtkWidget.update()

    ########################################################

    def creatVesselFromFile(self):
        for iV in range(self.nVessel):
            mPoints = vtk.vtkPoints()
            for iC in range(len(self.cirTab[iV])):
                mCir = vtk.vtkRegularPolygonSource()
                mCir.SetRadius(self.cirTab[iV][iC][3])
                mCir.GeneratePolygonOff()
                mCir.SetNumberOfSides(self.NPPC)

                mCir.SetNormal(1, 0, 0.0)
                mCir.SetCenter(0, 0, 0.0)
                mCir.Update()

                if iC == 0:
                    norm = self.norm3p(0, self.cirTab[iV][0], self.cirTab[iV][1], self.cirTab[iV][2])
                elif iC == len(self.cirTab[iV]) - 1:
                    norm = self.norm3p(2, self.cirTab[iV][iC - 2], self.cirTab[iV][iC - 1], self.cirTab[iV][iC])
                else:
                    norm = self.norm3p(1, self.cirTab[iV][iC - 1], self.cirTab[iV][iC], self.cirTab[iV][iC + 1])

                angle = vtk.vtkMath.AngleBetweenVectors([1, 0, 0], norm)
                axis = [0, 0, 0]
                vtk.vtkMath.Cross([1, 0, 0], norm, axis)

                mTransformFilter = vtk.vtkTransformFilter()
                mTransform = vtk.vtkTransform()
                #print(axis)
                mTransform.Translate(self.cirTab[iV][iC][0:3])
                mTransform.RotateWXYZ(angle / vtk.vtkMath.Pi() * 180., axis)

                mTransformFilter.SetTransform(mTransform)
                mTransformFilter.SetInputConnection(mCir.GetOutputPort())
                mTransformFilter.Update()

                mPoints.InsertPoints(mPoints.GetNumberOfPoints(), mCir.GetOutput().GetPoints().GetNumberOfPoints(), 0,
                                     mTransformFilter.GetOutput().GetPoints())

            mUnstructedGrid = vtk.vtkUnstructuredGrid()
            mUnstructedGrid.SetPoints(mPoints)
            print()
            for iC in range(len(self.cirTab[iV]) - 1):
                for iP in range(self.NPPC):
                    id0 = self.NPPC * iC + iP
                    id1 = (self.NPPC * iC + iP + 1) % self.NPPC + id0 // self.NPPC * self.NPPC
                    id3 = self.NPPC * iC + self.NPPC + iP
                    id2 = (self.NPPC * iC + self.NPPC + iP + 1) % self.NPPC + id3 // self.NPPC * self.NPPC
                    mUnstructedGrid.InsertNextCell(vtk.VTK_QUAD, 4, (int(id0), int(id1), int(id2), int(id3)))

            # Create a mapper
            mMapper = vtk.vtkDataSetMapper()
            mMapper.SetInputData(mUnstructedGrid)

            # Create an actor
            self.mActor = vtk.vtkActor()
            self.mActor.SetMapper(mMapper)
            self.mActor.GetProperty().SetColor(1, 0, 0)
            self.ren.AddActor(self.mActor)
            self.ren.ResetCamera()

    ########################################################

    def redrawVessel(self):
        #clear actor
        actors = self.ren.GetActors()
        tempActor = actors.GetLastActor()
        while tempActor != None:
            self.ren.RemoveActor(tempActor)
            tempActor = actors.GetLastActor()
        self.vtkWidget.update()

        #creat actor
        for iV in range(self.nVessel):
            mPoints = vtk.vtkPoints()
            print(self.blds[iV].getAArray())
            for iC in range(len(self.cirTab[iV])):
                mCir = vtk.vtkRegularPolygonSource()
                mCir.SetRadius(np.sqrt(self.blds[iV].getAArray()[iC]/np.pi/self.multiper))
                mCir.GeneratePolygonOff()
                mCir.SetNumberOfSides(self.NPPC)

                mCir.SetNormal(1, 0, 0.0)
                mCir.SetCenter(0, 0, 0.0)
                mCir.Update()

                if iC == 0:
                    norm = self.norm3p(0, self.cirTab[iV][0], self.cirTab[iV][1], self.cirTab[iV][2])
                elif iC == len(self.cirTab[iV]) - 1:
                    norm = self.norm3p(2, self.cirTab[iV][iC - 2], self.cirTab[iV][iC - 1], self.cirTab[iV][iC])
                else:
                    norm = self.norm3p(1, self.cirTab[iV][iC - 1], self.cirTab[iV][iC], self.cirTab[iV][iC + 1])

                angle = vtk.vtkMath.AngleBetweenVectors([1, 0, 0], norm)
                axis = [0, 0, 0]
                vtk.vtkMath.Cross([1, 0, 0], norm, axis)

                mTransformFilter = vtk.vtkTransformFilter()
                mTransform = vtk.vtkTransform()
                #print(axis)
                mTransform.Translate(self.cirTab[iV][iC][0:3])
                mTransform.RotateWXYZ(angle / vtk.vtkMath.Pi() * 180., axis)

                mTransformFilter.SetTransform(mTransform)
                mTransformFilter.SetInputConnection(mCir.GetOutputPort())
                mTransformFilter.Update()

                mPoints.InsertPoints(mPoints.GetNumberOfPoints(), mCir.GetOutput().GetPoints().GetNumberOfPoints(), 0,
                                     mTransformFilter.GetOutput().GetPoints())

            mUnstructedGrid = vtk.vtkUnstructuredGrid()
            mUnstructedGrid.SetPoints(mPoints)
            print()
            for iC in range(len(self.cirTab[iV]) - 1):
                for iP in range(self.NPPC):
                    id0 = self.NPPC * iC + iP
                    id1 = (self.NPPC * iC + iP + 1) % self.NPPC + id0 // self.NPPC * self.NPPC
                    id3 = self.NPPC * iC + self.NPPC + iP
                    id2 = (self.NPPC * iC + self.NPPC + iP + 1) % self.NPPC + id3 // self.NPPC * self.NPPC
                    mUnstructedGrid.InsertNextCell(vtk.VTK_QUAD, 4, (int(id0), int(id1), int(id2), int(id3)))

            # Create a mapper
            mMapper = vtk.vtkDataSetMapper()
            mMapper.SetInputData(mUnstructedGrid)

            # Create an actor
            self.mActor = vtk.vtkActor()
            self.mActor.SetMapper(mMapper)
            self.mActor.GetProperty().SetColor(1, 0, 0)
            self.ren.AddActor(self.mActor)
            self.ren.ResetCamera()

    ########################################################

    def compute(self):
        self.textBrowser.clear()
        if len(self.treeTab) == 0:
            print(self.treeTab)
            print (len(self.treeTab))
            self.textBrowser.append("请先导入网格!")
            return
        #self.textBrowser.append(".........  计算  .........")
        self.textBrowser.append("_________  状态  _________")
        self.textBrowser.append("平心血压（mmHg）：" + self.lineEdit_Pressure.text())
        
        self.textBrowser.append("心输出量（ml/s）：" + self.lineEdit_Flux.text())
        self.textBrowser.append("z方向过载：" + self.lineEdit_Gz.text())
        self.textBrowser.append("眼水平压（mmHg）：" + self.lineEdit_Pressure.text())
        self.textBrowser.append("心眼距离（mm）：" + self.lineEdit_Pressure.text())
        
        
        self.textBrowser.append("_________  结果  _________")
        self.blds = []
        for iV in range(self.nVessel):
            self.blds.append(blood.vessel())

            delts = [0]
            for iC in range(1, len(self.cirTab[iV])):
                delts.append(np.sqrt((self.cirTab[iV][iC][0] - self.cirTab[iV][iC - 1][0]) ** 2 + \
                                 (self.cirTab[iV][iC][1] - self.cirTab[iV][iC - 1][1]) ** 2 + \
                                 (self.cirTab[iV][iC][2] - self.cirTab[iV][iC - 1][2]) ** 2))
            s=[0]
            for iC in range(1, len(self.cirTab[iV])):
                s.append(s[iC-1] + delts[iC])


            #self.textBrowser.append('s Array')
            #self.textBrowser.append(str(s))

            a=[]
            for iC in range(0, len(self.cirTab[iV])):
                a.append(self.cirTab[iV][iC][3] ** 2 * np.pi * self.multiper**2)
            #self.textBrowser.append('a Array')
            #self.textBrowser.append(str(a))

            f=[]
            for iC in range(0, len(self.cirTab[iV])):
                f.append(self.cirTab[iV][iC][4])
            #self.textBrowser.append('f Array')
            #self.textBrowser.append(str(f))
            #self.textBrowser.append('\n')

            self.blds[-1].setSArray(s)
            self.blds[-1].setAArray(a)
            print("aArray:" + str(iV))
            print(a)
            self.blds[-1].setFArray(f)
            self.blds[-1].setP0(float(self.lineEdit_Pressure.text()) * 133)#mmHg -> Pa
            self.blds[-1].setE(600000)
            self.blds[-1].setG(9.8 * float(self.lineEdit_Gz.text()))


        # 计算第一段血管
        self.blds[0].setQ(float(self.lineEdit_Flux.text())*1e-6*1050.) #ml/s -> m3/s
        self.blds[0].setPin(float(self.lineEdit_Pressure.text())*133)#mmHg -> Pa
        self.blds[0].calculate2()

        # 依次计算各个分支
        for iTab in range(len(self.treeTab)):
            self.blds[self.treeTab[iTab][1]].setQ(self.blds[self.treeTab[iTab][0]].getQ() * self.treeTab[iTab][3])
            self.blds[self.treeTab[iTab][1]].setPin(self.blds[self.treeTab[iTab][0]].getPout())
            self.blds[self.treeTab[iTab][1]].calculate2()

            self.blds[self.treeTab[iTab][2]].setQ(self.blds[self.treeTab[iTab][0]].getQ() * self.treeTab[iTab][4])
            self.blds[self.treeTab[iTab][2]].setPin(self.blds[self.treeTab[iTab][0]].getPout())
            self.blds[self.treeTab[iTab][2]].calculate2()

        #print(self.treeTab)

        self.postProcess()
        self.redrawVessel()

    def postProcess(self):
        self.textBrowser.append("各段血管内流量(ml/s):")
        for ibld in range(len(self.blds)):
            self.textBrowser.append(str(ibld) + " : ")
            self.textBrowser.insertPlainText("%.2e" % (self.blds[ibld].getQ() / 1050. * 1000000))

        self.textBrowser.append("各段血管出口位置压强(mmHg):")
        for ibld in range(len(self.blds)):
            self.textBrowser.append(str(ibld) + " : ")
            self.textBrowser.insertPlainText("%.2f" % (self.blds[ibld].getPout()/133.))

        msum = 0.0
        for ibld in range(len(self.blds)):
            s = self.blds[ibld].getSArray()
            a = self.blds[ibld].getAArray()
            '''
            self.textBrowser.append("s")
            self.textBrowser.append(str(s))
            self.textBrowser.append("a")
            self.textBrowser.append(str(a))
            '''
            for iC in range(len(s) - 1):
                msum = msum + 0.5*(s[iC+1] - s[iC])*(a[iC+1]+a[iC])
        msum = msum / self.multiper**2

        self.textBrowser.append("血管储血量(ml):")
        self.textBrowser.append("%.2e" % (msum*1000000))


def main():
    app = Qt.QApplication(sys.argv)

    window = MainWindow()

    sys.exit(app.exec_())

if __name__ == "__main__":
    main()

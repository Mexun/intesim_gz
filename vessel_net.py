import blood_simple
from scipy.optimize import fsolve,leastsq


class vesselNet1:
    def __init__(self):
        pass

#
#                              ===============[q1, pout1]
#                            //
# [Q0, p1]  ===============[p2]
#                            \\
#                              ===============[q2, pout2]
#
#


def f(x):
    Q0 = 3.
    pout1 = 1.
    pout2 = 2.
    q1 = float(x[0])
    q2 = float(x[1])
    p1 = float(x[2])
    p2 = float(x[3])
    vessel0 = blood_simple.vessel()
    vessel1 = blood_simple.vessel()
    vessel2 = blood_simple.vessel()
    vessel0.setPout(p2)
    vessel1.setPout(pout1)
    vessel2.setPout(pout2)
    vessel0.setQ(Q0)
    vessel1.setQ(q1)
    vessel2.setQ(q2)
    vessel1.calculate()
    vessel2.calculate()
    vessel0.calculate()
    return [q1 + q2 - Q0,
            vessel1.getPin() - p2,
            vessel2.getPin() - p2,
            vessel0.getPin() - p1,
            ]


def solver1():
    print('hello vessel')
    result = fsolve(f,[1,1,2,3])
    print(result)
    print("result: ")
    print("Q1 : "),
    print(result[0])
    print("Q2 : "),
    print(result[1])
    print("P1 : "),
    print(result[2])
    print("P2 : "),
    print(result[3])


#
#                               ===============[q1, pout1]
#                             //
#                            //
# [Q0, pin]  ===============[pcross]
#                            \\
#                             \\
#                               ===============[q2, pout2]
#


def f2(x):
    Q0 = 3.
    pin = 2.
    q1 = float(x[0])
    q2 = float(x[1])
    pout1 = float(x[2])
    pout2 = float(x[3])
    pcross = float(x[4])
    vessel0 = blood_simple.vessel()
    vessel1 = blood_simple.vessel()
    vessel2 = blood_simple.vessel()
    vessel0.setPin(pin)
    vessel1.setPin(pcross)
    vessel2.setPin(pcross)
    vessel0.setQ(Q0)
    vessel1.setQ(q1)
    vessel2.setQ(q2)
    vessel1.calculate2()
    vessel2.calculate2()
    vessel0.calculate2()
    return [q1 + q2 - Q0,
            vessel1.getPout() - pout1,
            vessel2.getPout() - pout2,
            vessel0.getPout() - pcross,
            pout1-pout2
            ]



def main():
    solver1()


if __name__ == "__main__":
    main()
